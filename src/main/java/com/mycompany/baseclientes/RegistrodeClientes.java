/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.baseclientes;

import com.mycompany.baseclientes.dao.ClientesJpaController;
import com.mycompany.baseclientes.dao.exceptions.NonexistentEntityException;
import com.mycompany.baseclientes.entity.Clientes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("registroclientes")
public class RegistrodeClientes {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes() {

    /*Clientes cl1=new Clientes();
    cl1.setRut("17.115.035-3");
    cl1.setNombre("Constanza");
    cl1.setApellido("Hurtado");
    cl1.setEmail("constanza.hurtado@gmail.com");
    cl1.setCodproducto("123456");
    
    List<Clientes> lista =new ArrayList<>();
    lista.add(cl1);*/
       ClientesJpaController dao = new ClientesJpaController();

        List<Clientes> lista = dao.findClientesEntities();

        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idbuscar") String idbusca) {

        ClientesJpaController dao = new ClientesJpaController();

        Clientes tercerretiro = dao.findClientes(idbusca);
        System.out.print("rut devuelto" + tercerretiro.getRut());
        return Response.ok(200).entity(tercerretiro).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Clientes clientesregistro) {

        try {
            ClientesJpaController dao = new ClientesJpaController();
            dao.create(clientesregistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistrodeClientes.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(clientesregistro).build();

    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {

        try {
            ClientesJpaController dao = new ClientesJpaController();

            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RegistrodeClientes.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("cliente eliminado").build();
    }

    @PUT
    public Response update(Clientes clientesregistro) {

        try {
            ClientesJpaController dao = new ClientesJpaController();
            dao.edit(clientesregistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistrodeClientes.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(clientesregistro).build();
    }

}
