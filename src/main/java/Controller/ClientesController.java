/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.mycompany.baseclientes.dao.ClientesJpaController;
import com.mycompany.baseclientes.dao.exceptions.NonexistentEntityException;
import com.mycompany.baseclientes.entity.Clientes;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author const
 */
@WebServlet(name = "ClientesController", urlPatterns = {"/ClientesController"})
public class ClientesController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ClientesController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ClientesController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");
        String accion = request.getParameter("accion");
        if (accion.equals("ingreso")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String email = request.getParameter("email");
                String codigo = request.getParameter("codigo");

                Clientes clientes = new Clientes();
                clientes.setRut(rut);
                clientes.setNombre(nombre);
                clientes.setApellido(apellido);
                clientes.setEmail(email);
                clientes.setCodproducto(codigo);

                ClientesJpaController dao = new ClientesJpaController();
                dao.create(clientes);

            } catch (Exception ex) {
                Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (accion.equals("verlista")) {

            ClientesJpaController dao = new ClientesJpaController();
            List<Clientes> lista = dao.findClientesEntities();
            request.setAttribute("listaClientes", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }
        if (accion.equals("eliminar")) {

            try {
                String rut = request.getParameter("seleccion");
                ClientesJpaController dao = new ClientesJpaController();
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (accion.equals("consultar")) {
            String rut = request.getParameter("seleccion");
            ClientesJpaController dao = new ClientesJpaController();
            Clientes registro = dao.findClientes(rut);
            request.setAttribute("ver", registro);
            request.getRequestDispatcher("edicion.jsp").forward(request, response);

        }

        processRequest(request, response);
    }
        
        

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
